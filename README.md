ics-ans-jms2rdb
================

Ansible playbook to install [JMS2RDB](http://cs-studio.sourceforge.net/docbook/ch35.html).

Requirements
------------

- ansible >= 2.4
- molecule >= 2.6

License
-------

BSD 2-clause
